# Microservices Workshop
Lab 05: Updating a Single Service

---

## Instructions

### Update ui-service:

 - In the ui-service repository update the index.html to change the button text:
 
```
CALCULATE --> SEND
```

 - Run the ui-service job to create the new images
 
 - Run the deploy job to update the service